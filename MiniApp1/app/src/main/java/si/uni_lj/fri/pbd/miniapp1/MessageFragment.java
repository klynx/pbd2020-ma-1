package si.uni_lj.fri.pbd.miniapp1;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.navigation.NavigationView;

import java.util.Arrays;
import java.util.stream.Collectors;


public class MessageFragment extends Fragment {

    private GetChosenContacts chosenContactsListener;
    private Contact[] chosenArray;

    private final int EMAIL_REQUEST_CODE = 7456;
    private final int MMS_REQUEST_CODE = 2261;
    private boolean rotated = false;

    private static final String TAG = "MessageFragment";

    public interface GetChosenContacts {
        Contact[] getChosenContacts();
    }

    // check if activity implements GetChosenContacts interface
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

         try {
             chosenContactsListener = (GetChosenContacts) context;
         } catch (ClassCastException e) {
             throw new ClassCastException(context.toString() + " must implement GetChosenContacts interface");
         }
    }

    // retrieve chosen contacts in onResume, to assure that MainActivity.onRestoreInstanceState is called first
    // retrieving contacts earlier makes chosenArray null on rotation
    @Override
    public void onResume() {
        super.onResume();

        chosenArray = chosenContactsListener.getChosenContacts();
        if ((chosenArray == null || chosenArray.length < 1) && !rotated)
            sendLongToast(getString(R.string.no_chosen_warning));
        rotated = true;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        // if fragment was restarted due to rotation
        // set parameter so that the no contacts selected toast won't appear again
        if (savedInstanceState != null)
            rotated = savedInstanceState.getBoolean("rotation_check");

        //Log.d(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);

        try {
            assert getView() != null;
            Button emailButton = getView().findViewById(R.id.send_email);
            Button mmsButton = getView().findViewById(R.id.send_mms);
            emailButton.setOnClickListener(event -> sendGroupEmail());
            mmsButton.setOnClickListener(event -> sendGroupMMS());
        } catch(NullPointerException | AssertionError e) {
            Log.e(TAG, e.toString());
            sendLongToast(getString(R.string.init_fail));
        }
    }

    // save boolean value on rotation, so that the user isn't reminded that they
    // haven't chosen any contacts every time they rotate the screen
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putBoolean("rotation_check", rotated);
    }

    private void sendGroupEmail() {
        // turn contacts array into string of contact emails that the email app will parse
        String recipients = Arrays.stream(chosenArray)
                .filter(contact -> contact.email != null)
                .map(contact -> contact.email)
                .collect(Collectors.joining(";"));

        // check if any contacts don't have email information and message user that they will get removed
        if (Arrays.stream(chosenArray).anyMatch(contact -> contact.email == null))
            sendLongToast(getString(R.string.remove_no_email_contacts));

        // set up URI for the email app (no other method worked)
        String emailUriString = "mailto:" + recipients + "?subject=" + getString(R.string.email_subject) + "&body=" + getString(R.string.message_text);
        Uri emailUri = Uri.parse(emailUriString);

        try {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, emailUri);
            startActivityForResult(emailIntent, EMAIL_REQUEST_CODE);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            sendLongToast(getString(R.string.send_email_fail));
        }
    }

    private void sendGroupMMS() {

        // turn contacts array into string of contact numbers that the messaging app will parse
        String recipients = Arrays.stream(chosenArray)
                .filter(contact -> contact.number != null)
                .map(contact -> contact.number)
                .collect(Collectors.joining(";"));

        // check if any contacts don't have phone number information and message user that they will get removed
        if (Arrays.stream(chosenArray).anyMatch(contact -> contact.number == null))
            sendLongToast(getString(R.string.remove_no_number_contacts));

        // set up URI for the messaging app (no other method worked)
        String mmsUriString = "mms:" + recipients + "?body=" + getString(R.string.message_text) + "&exit_on_sent=" + true;
        Uri mmsUri = Uri.parse(mmsUriString);

        try {
            Intent mmsIntent = new Intent(Intent.ACTION_SENDTO, mmsUri);
            startActivityForResult(mmsIntent, MMS_REQUEST_CODE);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            sendLongToast(getString(R.string.send_message_fail));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int responseCode, Intent data) {
        super.onActivityResult(requestCode, responseCode, data);

        /*  If screen was rotated while messaging/email app was active the checked contacts array will be lost as this method is called before onResume.
            To prevent errors onResume is called if data wasn't yet retrieved.
            Calling goHome method below changes navigation back to HomeFragment, which ensures that onResume is never called twice.
         */
        if (chosenArray == null || chosenArray.length < 1)
            onResume();

        // on return from sending messages clear the checked contacts and go to home fragment
        if (requestCode == EMAIL_REQUEST_CODE || requestCode == MMS_REQUEST_CODE) {
            resetChecked();
            goHome();
        }
    }

    private void goHome() {
        try {
            NavigationView nv = getActivity().findViewById(R.id.nav_view);
            Menu menu = nv.getMenu();
            menu.performIdentifierAction(R.id.nav_home, 0);
        } catch (Exception e) {
            Log.e(TAG, "Error trying to direct view to HomeFragment" + e.toString());
        }
    }

    private void resetChecked() {
        Arrays.stream(chosenArray).forEach(contact -> contact.checked = false);
    }

    private void sendLongToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_message, container, false);
    }
}
