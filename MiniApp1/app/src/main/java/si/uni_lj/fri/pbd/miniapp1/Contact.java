package si.uni_lj.fri.pbd.miniapp1;

public class Contact {
    public String name;
    public String number;
    public String email;
    public boolean checked;

    Contact(String name, String number, String email) {
        this.name = name;
        this.number = number;
        this.email = email;
        this.checked = false;
    }

    public String toString() {
        return this.name;
    }

    public void toggle() {
        checked = !checked;
    }
}
