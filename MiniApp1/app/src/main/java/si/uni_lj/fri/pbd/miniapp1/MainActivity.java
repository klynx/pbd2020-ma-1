package si.uni_lj.fri.pbd.miniapp1;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.stream.IntStream;

public class MainActivity extends AppCompatActivity implements ContactsFragment.GetContactsData, MessageFragment.GetChosenContacts {

    private AppBarConfiguration mAppBarConfiguration;
    private ImageView profilePicture;
    private ArrayList<Contact> contacts;
    private final int APP_PERMISSION_REQUESTS = 1337;
    private final int CAMERA_PICTURE_REQUEST = 42069;
    private static final String TAG = "MainActivity";
    private boolean getContactsCheck = false;

    /*
        Override methods
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);

        navigationDrawerSetup(navigationView, drawer);
        initiateClassVariables(navigationView);

        // asks for permissions and handles retrieving contacts
        askPermissions();
        setProfilePicture();
        setProfilePictureOnClickEvent(navigationView);
    }

    // save which contacts were checked on rotation
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        // save intArray of checked states, where 1 == contact checked and 0 == contact not checked
        // using int array here, because java streams don't support collecting to boolean array
        savedInstanceState.putIntArray("contact_checked", contacts.stream()
                .mapToInt(contact -> contact.checked ? 1 : 0)
                .toArray());
    }

    // restore information about which contacts were chosen
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        int[] checkedValues = savedInstanceState.getIntArray("contact_checked");
        if (checkedValues == null) {
            Log.e(TAG, "Failed to restore state");
            return;
        }

        // check if contacts were added/removed while the application was suspended
        // don't update the list if they were
        if (checkedValues.length != contacts.size()) {
            Log.i(TAG, "Contacts and checkedValues list sized do not match. Previous state cannot be restored");
            sendLongToast(getString(R.string.number_of_contacts_change));
            return;
        }

        // update checked information to contacts
        IntStream.range(0, checkedValues.length)
                .forEach(i -> contacts.get(i).checked = checkedValues[i] == 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // if camera result, set and store new picture
        if (requestCode == CAMERA_PICTURE_REQUEST) {
            if (data == null) {
                Log.i(TAG, "No data was found in the return intent");
                sendLongToast(getString(R.string.update_image_fail));
                return;
            }

            Bundle dataExtras = data.getExtras();
            if (dataExtras == null || dataExtras.get("data") == null) {
                Log.i(TAG, "Picture data was not found. Exiting without updating picture");
                sendLongToast(getString(R.string.update_image_fail));
                return;
            }

            Bitmap picture = (Bitmap) dataExtras.get("data");
            profilePicture.setImageBitmap(picture);
            Log.i(TAG, "Picture was updated");
            sendLongToast(getString(R.string.update_image_success));

            // Store the image in shared preferences
            try {
                assert picture != null;
                SharedPreferences sharedAppPreferences = getAppSharedPreferences();
                String encodedImage = bitmap2Base64String(picture);
                sharedAppPreferences.edit().putString("profile_picture", encodedImage).apply();
                Log.i(TAG, "Picture saved to shared preferences");
            } catch (Exception e) {
                Log.e(TAG, "Saving picture failed with error: " + e.toString());
                sendLongToast(getString(R.string.save_image_fail));
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantedResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantedResults);

        // check if contacts permission was granted and get contacts if it was
        if (requestCode == APP_PERMISSION_REQUESTS) {
            int contactsPermissionIndex = IntStream.range(0, permissions.length)
                    .filter(i -> permissions[i].equals(Manifest.permission.READ_CONTACTS))
                    .findFirst()
                    .orElse(-1);
            if (contactsPermissionIndex != -1
                && grantedResults.length > 0
                && permissions[contactsPermissionIndex].equals(Manifest.permission.READ_CONTACTS)
                && grantedResults[contactsPermissionIndex] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG,"Contacts permission granted, getting contacts...");
                        getContacts();
                    return;
            }
            Log.i(TAG, "Contacts permission denied");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    /*
        Interface methods implementations
     */

    /**
     * ContactsFragment.GetContactsData
     * @return ArrayList of Contact class containing the retrieved contacts
     */
    public ArrayList<Contact> getContactsData() {
        Log.i("ContactsListener", "sending data to Fragment");
        return contacts;
    }

    /**
     * MessageFragment.GetChosenContacts
     * @return array of contacts that have been checked in the ContactsFragment
     */
    public Contact[] getChosenContacts() {
        // return array of only checked contacts
        return contacts.stream()
                .filter(contact -> contact.checked)
                .toArray(Contact[]::new);
    }

    /*
        Other Logic
     */

    private void navigationDrawerSetup(NavigationView navigationView, DrawerLayout drawer) {
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_contacts, R.id.nav_message)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    private void initiateClassVariables(NavigationView navigationView) {
        contacts = new ArrayList<Contact>();
        profilePicture = navigationView.getHeaderView(0).findViewById(R.id.profilePicture);
    }

    private SharedPreferences getAppSharedPreferences() {
        return getApplicationContext().getSharedPreferences("appPreferences", MODE_PRIVATE);
    }

    // set profile picture from sharedPreferences when activity is created
    private void setProfilePicture() {
        SharedPreferences appPreferences = getAppSharedPreferences();

        if (appPreferences == null) {
            Log.e(TAG, "Shared Preferences could not be retrieved");
            return;
        }
        Log.i(TAG, "Shared preferences retrieved successfully");

        String encodedImage = appPreferences.getString("profile_picture", "");
        if (encodedImage == null || encodedImage.equals("")) {
            Log.i(TAG, "Profile picture was not found in shared preferences");
            return;
        }

        Log.i(TAG, "Profile picture retrieved from shared preferences");

        Bitmap image = base64String2Bitmap(encodedImage);
        if (image == null) {
            Log.e(TAG, "Profile picture decoded to nothing");
            return;
        }
        Log.i(TAG, "Picture decoded successfully");

        if (profilePicture == null) {
            Log.i(TAG, "Profile picture ImageView could not be found");
            return ;
        }

        profilePicture.setImageBitmap(image);
        Log.i(TAG, "Successfully updated profile picture from shared preferences");
    }

    private void setProfilePictureOnClickEvent(NavigationView navigationView) {
        if (profilePicture == null) {
            Log.e(TAG,"profilePicture is null. Cannot append onClick listener to profile picture.");
            return;
        }

        profilePicture.setOnClickListener(event -> {
            try {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_PICTURE_REQUEST);
            } catch (Exception e) {
                Log.e(TAG, "Launching camera app for result failed " + e.toString());
                sendLongToast(getString(R.string.launch_camera_fail));
            }
        });
    }

    /**
     * encodes a bitmap image into a base64 string
     * @param image Bitmap image
     * @return image encoded into a Base64 string
     */
    private String bitmap2Base64String(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    /**
     * decodes base64 string to bitmap image
     * @param encodedImage encoded image string
     * @return decoded Bitmap image
     */
    private Bitmap base64String2Bitmap(String encodedImage) {
        byte[] b = Base64.decode(encodedImage, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(b, 0, b.length);
    }

    private void sendLongToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private void askPermissions() {
        ArrayList<String> permissionRequests = new ArrayList<String>();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS ) != PackageManager.PERMISSION_GRANTED) {
            permissionRequests.add(Manifest.permission.READ_CONTACTS);
        }
        else {
            Log.i(TAG, "Permission to read contacts is already granted. Fetching contacts...");
            if (!getContactsCheck)
                getContacts();
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA ) != PackageManager.PERMISSION_GRANTED) {
            permissionRequests.add(Manifest.permission.CAMERA);
        }

        if (permissionRequests.size() > 0) {
            ActivityCompat.requestPermissions(this, permissionRequests.toArray(new String[0]), APP_PERMISSION_REQUESTS);
            Log.i(TAG, "sending " + permissionRequests.size() + " permission requests");
        }
    }

    // get contacts and store them in contacts ArrayList
    private void getContacts()  {
        if (contacts == null) {
            Log.e(TAG, "Contacts ArrayList is null, cannot store contacts");
            sendLongToast(getString(R.string.get_contacts_fail));
            return;
        }

        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if (cur == null) {
            Log.e(TAG, "Could not get cursor");
            sendLongToast(getString(R.string.get_contacts_fail));
            return;
        }

        while (cur.moveToNext()) {
            String contactId = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));

            String contactNumber = getContactNumber(contactId);
            String contactEmail = getContactEmail(contactId);
            String contactName = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

            Contact contact = new Contact(contactName, contactNumber, contactEmail);
            contacts.add(contact);
        }
        Log.i(TAG, "Retrieved contacts.");
        cur.close();

        //sort contacts list alphabetically
        contacts.sort((contact1, contact2) -> contact1.name.compareTo(contact2.name));
        getContactsCheck = true;
    }

   private String getContactNumber (String id) {
        Cursor phoneCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id, null, null);
        if (phoneCursor == null) {
            return null;
        }
        if(phoneCursor.moveToFirst()) {
            return phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
        }
        phoneCursor.close();
        return null;
   }

   private String getContactEmail (String id) {
       Cursor emailCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + id, null, null);
       if (emailCursor == null) {
           return null;
       }
       if (emailCursor.moveToFirst())  {
           return emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
       }
       emailCursor.close();
       return null;
   }
}