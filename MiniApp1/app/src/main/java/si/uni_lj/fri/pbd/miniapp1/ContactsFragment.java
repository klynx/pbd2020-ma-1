package si.uni_lj.fri.pbd.miniapp1;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.fragment.app.ListFragment;

import java.util.ArrayList;
import java.util.stream.IntStream;



public class ContactsFragment extends ListFragment {

    private static final String TAG = "ListFragment";

    private ArrayList<Contact> contactArrayList;
    private GetContactsData contactsDataListener;

    public interface GetContactsData {
        ArrayList<Contact> getContactsData();
    }

    // check if activity implements GetContactsData interface
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            contactsDataListener = (GetContactsData) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement GetContactData interface");
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        // create list
        // this is done in onResume, so that if the contact permission is
        // accepted inside the contacts tab, the list is still instantly displayed
        getDataAndSetupList();
    }

    private void getDataAndSetupList() {
        contactArrayList = contactsDataListener.getContactsData();

        if (contactArrayList == null || contactArrayList.size() < 1) {
            Log.i(TAG, "Didn't get any contacts from activity");
            sendLongToast(getString(R.string.no_contacts_toast));
            setListAdapter(null);
            return;
        }

        try {
            assert getActivity() != null;
            ArrayAdapter<Contact> contactsAdapter = new ArrayAdapter<Contact>(getActivity(), android.R.layout.select_dialog_multichoice, contactArrayList);
            // make list change if ArrayList changes
            contactsAdapter.notifyDataSetChanged();
            contactsAdapter.setNotifyOnChange(true);

            setListAdapter(contactsAdapter);
            getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

            // set checkbox chosen/not chosen for each contact
            setChecksInList();

            // make sure that clicking on item changes the checked value in Contact
            getListView().setOnItemClickListener((parent, view, index, id) -> contactArrayList.get(index).toggle());
        } catch (NullPointerException | AssertionError e) {
            Log.e(TAG, "Error occurred trying to set the listView: " + e.toString());
            sendLongToast(getString(R.string.contacts_list_setup_failed));
            setListAdapter(null);
        }

    }

    private void setChecksInList() {
        // check the checkbox in chosen contacts
        IntStream.range(0, contactArrayList.size())
                .forEach(i -> getListView().setItemChecked(i, contactArrayList.get(i).checked));
    }

    private void sendLongToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }
}
